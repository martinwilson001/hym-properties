<?php include('includes/header.php');?>


<section class="contactus-wrap">
    <img class="contact-shape" src="assets/images/shape.png">
    <div class="container">
        <div class="contact-container">
            <h1>ERROR 404</h1>
            <ul>
                <li class="active"><a href="index.php">Home</a></li>
                <li>Error</li>
            </ul>
        </div>
    </div>
</section>

<section class="error-main-wrap">
    <div class="container">
        <img class="error-image" src="assets/images/error.png">
        <button><img src="assets/images/arrow-long.svg"><a href="">Back To Home</a></button>
    </div>
</section>

<?php include('includes/footer.php');?>