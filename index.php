<?php include('includes/header.php');?>
<section class="banner-wrap"  id="index">
    <div class="banner-content-wrap">
        <h1>LET US FIND YOUR <span>DREAM...</span></h1>
        <div class="banner-form-wrap">
            <form>
                <div class="custom-form-wrap">
                    <div class="form-group">
                        <label>Looking For?</label>
                        <input type="text" placeholder="Rent a house">
                        <span>6</span>
                    </div>
                    <div class="form-group">
                        <label>City</label>
                        <input type="text" placeholder="Dubai">
                        <span>6</span>
                    </div>
                    <div class="form-group">
                        <label>Rent a price</label>
                        <input type="text" placeholder="75K-95K">
                        <span>6</span>
                    </div>
                </div>
                <button>SEARCH</button>
            </form>
        </div>
    </div>
</section>

<section class="aboutus-main-wrap" id="aboutus">
    <span class="white-triangle"></span>
    <span class="white-rectangle"></span>
    <span class="home-icon"><img src="assets/images/home-icon.png"></span>
    <div class="container">
        <div class="aboutus-wrap">
            <div class="aboutus-right-wrap">
                <div class="aboutus-pic-main-wrap">
                    <div class="aboutus-pic-wrap">
                        <img src="assets/images/agent.png">
                        <div class="aboutus-pic-title">
                            <h3>Our Agents</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                    </div>
                </div>
                <div class="aboutus-pic-main-wrap">
                    <div class="aboutus-pic-wrap mb-30">
                        <img src="assets/images/markect.png">
                        <div class="aboutus-pic-title">
                            <h3>Marketing & Innovation</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                    </div>
                    <div class="aboutus-pic-wrap">
                        <img src="assets/images/partners.png">
                        <div class="aboutus-pic-title">
                            <h3>Our Partners</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="aboutus-left-wrap">
                <h2>WHO WE <br /> ARE</h2>
                <p class="pb-30">A subsidiary of the HYM Investment Group, HYM Properties was established in 2017. An
                    Emirati owned, boutique Dubai-based real estate company which prides itself on long lasting
                    relationships and being recognised as industry experts.</p>
                <p class="pb-30"> Always at the forefront of market trends, HYM Properties takes a partnership approach
                    where clients directly benefit from wide-ranging experience and a unique deep-rooted country
                    network. Underpinned by international standards and quality management systems, we proudly focus on
                    providing results through professional, client centric solutions.</p>
                <button>Read More<img src="assets/images/arrow-long.svg"></button>
            </div>
        </div>
    </div>
</section>

<section class="featured-properties-wrap" >
    <div class="container">
        <h2>FEATURED PROPERTIES</h2>
        <p>Lorem ipsum dolor sit amet,Stet clita kasd gubergren, no sea takimata sanctus est is the lorem ipsum dolor
            sit amet. sed diam nonumy ut labore et dolore s the magna aliquyam.</p>
        <ul class="property-select">
            <li class="active">Rent</li>
            <li>Sell</li>
            <li>Buy</li>
        </ul>
        <div class="properties-main-wrap">
            <div class="properties-wrap">
                <img src="assets/images/property.png">
                <div class="property-content-wrap">
                    <h3>Apt P101, Dubai</h3>
                    <div class="property-aminities">
                        <ul>
                            <li><img src="assets/images/bed.svg">2bd</li>
                            <li><img src="assets/images/bath.svg">2ba</li>
                        </ul>
                        <button>AED 65K</button>
                    </div>
                </div>
            </div>
            <div class="properties-wrap">
                <img src="assets/images/property.png">
                <div class="property-content-wrap">
                    <h3>Apt P101, Dubai</h3>
                    <div class="property-aminities">
                        <ul>
                            <li><img src="assets/images/bed.svg">2bd</li>
                            <li><img src="assets/images/bath.svg">2ba</li>
                        </ul>
                        <button>AED 65K</button>
                    </div>
                </div>
            </div>
            <div class="properties-wrap">
                <img src="assets/images/property.png">
                <div class="property-content-wrap">
                    <h3>Apt P101, Dubai</h3>
                    <div class="property-aminities">
                        <ul>
                            <li><img src="assets/images/bed.svg">2bd</li>
                            <li><img src="assets/images/bath.svg">2ba</li>
                        </ul>
                        <button>AED 65K</button>
                    </div>
                </div>
            </div>
            <div class="properties-wrap">
                <img src="assets/images/property.png">
                <div class="property-content-wrap">
                    <h3>Apt P101, Dubai</h3>
                    <div class="property-aminities">
                        <ul>
                            <li><img src="assets/images/bed.svg">2bd</li>
                            <li><img src="assets/images/bath.svg">2ba</li>
                        </ul>
                        <button>AED 65K</button>
                    </div>
                </div>
            </div>
            <div class="properties-wrap">
                <img src="assets/images/property.png">
                <div class="property-content-wrap">
                    <h3>Apt P101, Dubai</h3>
                    <div class="property-aminities">
                        <ul>
                            <li><img src="assets/images/bed.svg">2bd</li>
                            <li><img src="assets/images/bath.svg">2ba</li>
                        </ul>
                        <button>AED 65K</button>
                    </div>
                </div>
            </div>
            <div class="properties-wrap">
                <img src="assets/images/property.png">
                <div class="property-content-wrap">
                    <h3>Apt P101, Dubai</h3>
                    <div class="property-aminities">
                        <ul>
                            <li><img src="assets/images/bed.svg">2bd</li>
                            <li><img src="assets/images/bath.svg">2ba</li>
                        </ul>
                        <button>AED 65K</button>
                    </div>
                </div>
            </div>
            <div class="property-btn-wrap">
                <button>View All<img src="assets/images/arrow-long.svg"></button>
            </div>
        </div>
    </div>
</section>

<section class="aboutus-main-wrap feature-stripe-wrap" id="feaured">
    <div class="container">
        <h2>Features & Benifits</h2>
        <div class="aboutus-wrap">
            <div class="aboutus-left-wrap">
                <h3>Our Clients Enjoy <br /> This Benefits</h3>
                <p class="pb-30">Our services are designed with the objective of enhancing our clients’ asset value and
                    maximizing return on investment. We benchmark ourselves against the best in the business globally,
                    and consistently strive to achieve and maintain international standards and best practices in
                    delivering services.</p>
                <button>View All<img src="assets/images/arrow-long.svg"></button>
            </div>
            <div class="aboutus-right-wrap">
                <div class="aboutus-pic-main-wrap">
                    <div class="aboutus-pic-wrap mb-30 active">
                        <img src="assets/images/time.svg">
                        <h3>Time Efficient</h3>
                        <p>Our clients enjoy real time reporting on their property portfolio status including financial
                            performance and occupancy.</p>
                    </div>
                    <div class="aboutus-pic-wrap">
                        <img src="assets/images/target.svg">
                        <h3>Target The Best</h3>
                        <p>We professionally introduce qualified buyers and tenants to our clients’ properties in an
                            effective and prompt manner.</p>
                    </div>
                </div>
                <div class="aboutus-pic-main-wrap">
                    <div class="aboutus-pic-wrap">
                        <img src="assets/images/support.svg">
                        <h3>Support Team</h3>
                        <p>Our team provides easy, stress-free solutions, making sure your investment is always
                            performing at its maximum and in the right hands.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-services-main-wrap" id="our-services">
    <h2>Our Services</h2>
    <div class="container">
        <div class="our-service-wrap">
            <div class="service-block">
                <span><img src="assets/images/residence.png"></span>
                <p>RESIDENTIAL SALES & LEASING</p>
            </div>
            <div class="service-block">
                <span><img src="assets/images/building.png"></span>
                <p>COMMERCIAL SALES & LEASING</p>
            </div>
            <div class="service-block">
                <span><img src="assets/images/home.png"></span>
                <p>PROPERTY MANAGEMENT</p>
            </div>
            <div class="service-block">
                <span><img src="assets/images/job.png"></span>
                <p>PORTFOLIO &ASSET MANAGEMENT</p>
            </div>
            <div class="service-block">
                <span><img src="assets/images/socialmedia.png"></span>
                <p>MARKETING & COMMUNICATIONS</p>
            </div>
        </div>
    </div>
</section>

<section class="buy-sell-property-main-wrap" id="contact">
    <div class="container">
        <div class="sell-buy-property-wrap">
            <h2>Want to Buy or Sell Your Property?</h2>
            <p>We offer a full suite of real estate services to individual property owners through to managed mixed use
                property portfolios for developers, institutional investors, and large asset owners.</p>
            <div class="sell-buy-button-property-wrap">
                <button>Buying Property</button>
                <span></span>
                <button>Selling Property</button>
            </div>
        </div>
    </div>
</section>



<?php include('includes/footer.php');?>