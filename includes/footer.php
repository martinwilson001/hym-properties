<footer>
        <div class="container">
            <div class="footer-wrap">
                <div class="footer-logo-wrap">
                    <img src="assets/images/white-logo.svg">
                    <ul>
                        <li><a href=""><img src="assets/images/facebook.svg"></a></li>
                        <li><a href=""><img src="assets/images/instagram.svg"></a></li>
                        <li><a href=""><img src="assets/images/facebook.svg"></a></li>
                    </ul>
                </div>
                <div class="quick-link-wrap">
                    <h3>Quick Links</h3>
                    <ul>
                        <li><a href="">- Home</a></li>
                        <li><a href="">- About Us</a></li>
                        <li><a href="">- Featured</a></li>
                        <li><a href="">- Our Services</a></li>
                        <li><a href="">- Contact Us</a></li>
                    </ul>
                </div>
                <div class="quick-link-wrap">
                    <h3>Sitemap</h3>
                    <ul>
                        <li><a href="">- Our Process</a></li>
                        <li><a href="">- What We Do</a></li>
                        <li><a href="">- Location</a></li>
                        <li><a href="">- Sell</a></li>
                        <li><a href="">- Purchase</a></li>
                    </ul>
                </div>
                <div class="quick-link-wrap">
                    <h3>Discover</h3>
                    <ul>
                        <li><a href="">- Privacy Policy</a></li>
                        <li><a href="">- Terms & Conditions</a></li>
                        <li><a href="">- Featured</a></li>
                        <li><a href="">- Owners</a></li>
                        <li><a href="">- Houses</a></il>
                    </ul>
                </div>
                <div class="quick-link-wrap subscribe-wrap">
                    <h3>Subscribe</h3>
                    <form>
                        <div class="form-group">
                            <input type="email" placeholder="Email Address">
                            <button>Subscribe</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="footer-copy-write">
                <p>Copyright © 2022 HYM Properties. All rights reserved.</p>
            </div>
        </div>
    </footer>

    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="assets/js/main.js"></script>


</body>
</html>