<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HYM Properties</title>
    <link rel="icon" type="image/x-icon" href="assets/images/favicon.ico">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/main.css">

</head>
<body>
    <header>
        <div id="nav-icon">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="overflow"></div>
        <div class="container">
            <div class="header-wrap">
                <div class="logo-wrap">
                    <img src="assets/images/logo.svg">
                </div>
                <div class="nav-wrap">
                    <nav>
                        <ul>
                            <li><a href="#index" class="active">Home</a></li>
                            <li><a href="#aboutus">About Us</a></li>
                            <li><a href="#feaured">Featured</a></li>
                            <li><a href="#our-services">Our Servcies</a></li>
                            <li><a href="contact.php">Contact Us</a></li>
                            <li class="contact-btn"><a href="tel:+97145540616"><img src="assets/images/phone.svg">+97145540616</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>