<?php include('includes/header.php');?>

<section class="contactus-wrap">
    <img class="contact-shape" src="assets/images/shape.png">
    <div class="container">
        <div class="contact-container">
            <h1>Contact Us</h1>
            <ul>
                <li class="active"><a href="index.php">Home</a></li>
                <li>Contact</li>
            </ul>
        </div>
    </div>
</section>

<section class="contact-form-main-wrap">
    <div class="container">
        <div class="contact-container">
            <div class="contact-item-wrap">
                <div class="contact-item">
                    <div class="contact-item-right">
                        <span><img src="assets/images/facebook.svg"></span>
                    </div>
                    <div class="contact-item-left">
                        <h3>Phone:</h3>
                        <p><a href="tel:+971 4 554 0616">+971 4 554 0616</a></p>
                        <p><a href="tel:+971 52 167 5959">+971 52 167 5959</a></p>
                    </div>
                </div>
                <div class="contact-item">
                    <div class="contact-item-right">
                        <span><img src="assets/images/facebook.svg"></span>
                    </div>
                      
                    <div class="contact-item-left">
                        <h3>Email:</h3>
                        <p><a href="mailto:info@hym.ae">info@hym.ae</a></p>
                        <p><a href="mailto:support@hym.ae">support@hym.ae</a></p>
                    </div>
                </div>
                <div class="contact-item">
                    <div class="contact-item-right">
                        <span><img src="assets/images/facebook.svg"></span>
                    </div>
                     
                    <div class="contact-item-left">
                        <h3>Address:</h3>
                        <p>32nd Floor, Al Saqr Business Tower,</p>
                        <p> Sheikh Zayed Road, Dubai, UAE</p>
                    </div>
                </div>
            </div>
            <div class="contact-form-wrap">
                <h3>Interested in discussing? <br/> Contact Now.</h3>
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore fugiat sunt culpa officia deserunt mollit anim est laborum.</p>
                <form>
                    <div class="contact-form-right">
                        <div class="form-group">
                            <input type="text" placeholder="Name:">
                        </div>
                        <div class="form-group">
                            <input type="email" placeholder="Email:">
                        </div>
                        <div class="form-group">
                            <input type="number" placeholder="Phone:">
                        </div>
                    </div>
                    <div class="contact-form-left">
                    <div class="form-group">
                           <textarea placeholder="Message:" rows="9"></textarea>
                        </div>
                        <button>Send Message</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="map-section">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15571892.376418732!2d56.71084710807317!3d17.66286965954487!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f428c4b20d9c1%3A0xda2a93cfee3dee03!2sAl%20Saqr%20Business%20Tower!5e0!3m2!1sen!2sin!4v1649603973114!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</section>

<?php include('includes/footer.php');?>