$(document).ready(function(){
	$('#nav-icon').click(function(){
		$(this).toggleClass('open');
        $(this).parents('html').addClass('overflow-open');
	});

});

$(document).on("click",".overflow",function(){
    $('#nav-icon').removeClass('open');
    $(this).parents('html').removeClass('overflow-open');
});

$(document).on("click","header .header-wrap .nav-wrap nav ul li a",function(){
    $(this).parents('ul').find('.active').removeClass('active');
    $(this).addClass('active');
    $('#nav-icon').removeClass('open');
    $(this).parents('html').removeClass('overflow-open');
});




$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 100) {
        $("header").addClass("darkheader");
    } else {
        $("header").removeClass("darkheader");
    }
}),scroll();